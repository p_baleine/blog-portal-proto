require 'spec_helper'

describe 'posts/index.html.erb' do
  let(:post) { mock_model('Post').as_new_record.as_null_object }

  it 'displays posts' do
    assign(:posts, [post, post])
    render
    assert_select '.post', count: 2
  end

  it 'displays posts with its title' do
    post.stub(:title).and_return('My Blog Post')
    assign(:posts, [post])
    render
    assert_select '.post td.title', text: 'My Blog Post'
  end
end

#### prepare rails with rspec
    ~ $ cd Documents/github/blog-portal-proto/
    ~/Documents/github/blog-portal-proto $ cat <<EOF > Gemfile
    > source 'http://rubygems.org'
    > gem 'rails', '~> 3.0.0'
    > EOF
    ~/Documents/github/blog-portal-proto $ bundle install --path vendor/bundler
    Fetching gem metadata from http://rubygems.org/...........
    Fetching gem metadata from http://rubygems.org/..
    Installing rake (0.9.2.2) 
    Installing abstract (1.0.0) 
    Installing activesupport (3.0.17) 
    Installing builder (2.1.2) 
    Installing i18n (0.5.0) 
    Installing activemodel (3.0.17) 
    Installing erubis (2.6.6) 
    Installing rack (1.2.5) 
    Installing rack-mount (0.6.14) 
    Installing rack-test (0.5.7) 
    Installing tzinfo (0.3.33) 
    Installing actionpack (3.0.17) 
    Installing mime-types (1.19) 
    Installing polyglot (0.3.3) 
    Installing treetop (1.4.11) 
    Installing mail (2.2.19) 
    Installing actionmailer (3.0.17) 
    Installing arel (2.0.10) 
    Installing activerecord (3.0.17) 
    Installing activeresource (3.0.17) 
    Using bundler (1.1.4) 
    Installing json (1.7.5) with native extensions 
    Installing rdoc (3.12) 
    Installing thor (0.14.6) 
    Installing railties (3.0.17) 
    Installing rails (3.0.17) 
    Your bundle is complete! It was installed into ./vendor/bundler
    Post-install message from rdoc:
    Depending on your version of ruby, you may need to install ruby rdoc/ri data:
    
    <= 1.8.6 : unsupported
     = 1.8.7 : gem install rdoc-data; rdoc-data --install
     = 1.9.1 : gem install rdoc-data; rdoc-data --install
    >= 1.9.2 : nothing to do! Yay!
    
    ~/Documents/github/blog-portal-proto $ bundle exec rails new .
           exist  
          create  README
          create  Rakefile
          create  config.ru
          create  .gitignore
        conflict  Gemfile
    Overwrite /home/tajima-junpei/Documents/github/blog-portal-proto/Gemfile? (enter "h" for help) [Ynaqdh] 
           force  Gemfile
          create  app
          create  app/controllers/application_controller.rb
          create  app/helpers/application_helper.rb
          create  app/mailers
          create  app/models
          create  app/views/layouts/application.html.erb
          create  config
          create  config/routes.rb
          create  config/application.rb
          create  config/environment.rb
          create  config/environments
          create  config/environments/development.rb
          create  config/environments/production.rb
          create  config/environments/test.rb
          create  config/initializers
          create  config/initializers/backtrace_silencers.rb
          create  config/initializers/inflections.rb
          create  config/initializers/mime_types.rb
          create  config/initializers/secret_token.rb
          create  config/initializers/session_store.rb
          create  config/locales
          create  config/locales/en.yml
          create  config/boot.rb
          create  config/database.yml
          create  db
          create  db/seeds.rb
          create  doc
          create  doc/README_FOR_APP
          create  lib
          create  lib/tasks
          create  lib/tasks/.gitkeep
          create  log
          create  log/server.log
          create  log/production.log
          create  log/development.log
          create  log/test.log
          create  public
          create  public/404.html
          create  public/422.html
          create  public/500.html
          create  public/favicon.ico
          create  public/index.html
          create  public/robots.txt
          create  public/images
          create  public/images/rails.png
          create  public/stylesheets
          create  public/stylesheets/.gitkeep
          create  public/javascripts
          create  public/javascripts/application.js
          create  public/javascripts/controls.js
          create  public/javascripts/dragdrop.js
          create  public/javascripts/effects.js
          create  public/javascripts/prototype.js
          create  public/javascripts/rails.js
          create  script
          create  script/rails
          create  test
          create  test/fixtures
          create  test/functional
          create  test/integration
          create  test/performance/browsing_test.rb
          create  test/test_helper.rb
          create  test/unit
          create  tmp
          create  tmp/sessions
          create  tmp/sockets
          create  tmp/cache
          create  tmp/pids
          create  vendor/plugins
          create  vendor/plugins/.gitkeep
    ~/Documents/github/blog-portal-proto $ echo "gem 'rspec-rails', group: [:development, :test]" >> Gemfile
    ~/Documents/github/blog-portal-proto $ bundle install
    Fetching gem metadata from http://rubygems.org/.........
    Fetching gem metadata from http://rubygems.org/..
    Using rake (0.9.2.2) 
    Using abstract (1.0.0) 
    Using activesupport (3.0.17) 
    Using builder (2.1.2) 
    Using i18n (0.5.0) 
    Using activemodel (3.0.17) 
    Using erubis (2.6.6) 
    Using rack (1.2.5) 
    Using rack-mount (0.6.14) 
    Using rack-test (0.5.7) 
    Using tzinfo (0.3.33) 
    Using actionpack (3.0.17) 
    Using mime-types (1.19) 
    Using polyglot (0.3.3) 
    Using treetop (1.4.11) 
    Using mail (2.2.19) 
    Using actionmailer (3.0.17) 
    Using arel (2.0.10) 
    Using activerecord (3.0.17) 
    Using activeresource (3.0.17) 
    Using bundler (1.1.4) 
    Installing diff-lcs (1.1.3) 
    Using json (1.7.5) 
    Using rdoc (3.12) 
    Using thor (0.14.6) 
    Using railties (3.0.17) 
    Using rails (3.0.17) 
    Installing rspec-core (2.11.1) 
    Installing rspec-expectations (2.11.3) 
    Installing rspec-mocks (2.11.3) 
    Installing rspec (2.11.0) 
    Installing rspec-rails (2.11.4) 
    Installing sqlite3 (1.3.6) with native extensions 
    Your bundle is complete! It was installed into ./vendor/bundler
    ~/Documents/github/blog-portal-proto $ bundle exec rails generate rspec:install
          create  .rspec
          create  spec
          create  spec/spec_helper.rb
    ~/Documents/github/blog-portal-proto $ bundle exec rake db:migrate
    ~/Documents/github/blog-portal-proto $ mkdir -p spec/views/posts
    ~/Documents/github/blog-portal-proto $ echo '--format documentation' >> .rspec 
    
#### spec for views
    ~/Documents/github/blog-portal-proto $ # emcacs spec/views/posts/show.html.erb_spec.rb
    ~/Documents/github/blog-portal-proto $ bundle exec rake spec
    /usr/local/bin/ruby -S rspec ./spec/views/posts/show.html.erb_spec.rb
    F
    
    Failures:
    
      1) posts/show.html.erb displays the description attribute of the post
         Failure/Error: render
         ActionView::MissingTemplate:
           Missing template posts/show with {:handlers=>[:erb, :rjs, :builder, :rhtml, :rxml], :formats=>[:html, :text, :js, :css, :ics, :csv, :xml, :rss, :atom, :yaml, :multipart_form, :url_encoded_form, :json], :locale=>[:en, :en]} in view paths "/home/tajima-junpei/Documents/github/blog-portal-proto/app/views"
         # ./spec/views/posts/show.html.erb_spec.rb:5:in `block (2 levels) in <top (required)>'
    
    Finished in 0.03511 seconds
    1 example, 1 failure
    
    Failed examples:
    
    rspec ./spec/views/posts/show.html.erb_spec.rb:4 # posts/show.html.erb displays the description attribute of the post
    
    Randomized with seed 2703
    
    rake aborted!
    /usr/local/bin/ruby -S rspec ./spec/views/posts/show.html.erb_spec.rb failed
    
    Tasks: TOP => spec
    (See full trace by running task with --trace)
    ~/Documents/github/blog-portal-proto $ mkdir -p app/views/posts
    ~/Documents/github/blog-portal-proto $ touch app/views/posts/show.html.erb
    ~/Documents/github/blog-portal-proto $ bundle exec rake spec
    /usr/local/bin/ruby -S rspec ./spec/views/posts/show.html.erb_spec.rb
    F
    
    Failures:
    
      1) posts/show.html.erb displays the description attribute of the post
         Failure/Error: rendered.should contain('we launched Google Affiliate Ads for Blogger in the US')
         NoMethodError:
           undefined method `contain' for #<RSpec::Core::ExampleGroup::Nested_1:0x00000002292168>
         # ./spec/views/posts/show.html.erb_spec.rb:6:in `block (2 levels) in <top (required)>'
    
    Finished in 0.03361 seconds
    1 example, 1 failure
    
    Failed examples:
    
    rspec ./spec/views/posts/show.html.erb_spec.rb:4 # posts/show.html.erb displays the description attribute of the post
    
    Randomized with seed 13697
    
    rake aborted!
    /usr/local/bin/ruby -S rspec ./spec/views/posts/show.html.erb_spec.rb failed
    
    Tasks: TOP => spec
    (See full trace by running task with --trace)
    ~/Documents/github/blog-portal-proto $ # emacs views/posts/show.html.erb
    ~/Documents/github/blog-portal-proto $ bundle exec rake spec
    /usr/local/bin/ruby -S rspec ./spec/views/posts/show.html.erb_spec.rb
    F
    
    Failures:
    
      1) posts/show.html.erb displays the description attribute of the post
         Failure/Error: rendered.should contain('we launched Google Affiliate Ads for Blogger in the US')
         NoMethodError:
           undefined method `contain' for #<RSpec::Core::ExampleGroup::Nested_1:0x00000002b7abe0>
         # ./spec/views/posts/show.html.erb_spec.rb:6:in `block (2 levels) in <top (required)>'
    
    Finished in 0.0354 seconds
    1 example, 1 failure
    
    Failed examples:
    
    rspec ./spec/views/posts/show.html.erb_spec.rb:4 # posts/show.html.erb displays the description attribute of the post
    
    Randomized with seed 43127
    
    rake aborted!
    /usr/local/bin/ruby -S rspec ./spec/views/posts/show.html.erb_spec.rb failed
    
    Tasks: TOP => spec
    (See full trace by running task with --trace)
    ~/Documents/github/blog-portal-proto $ bundle exec rake spec
    /usr/local/bin/ruby -S rspec ./spec/views/posts/show.html.erb_spec.rb
    
    posts/show.html.erb
      displays the description attribute of the post
    
    Finished in 0.03634 seconds
    1 example, 0 failures
    
    Randomized with seed 38065
    
    ~/Documents/github/blog-portal-proto $ 